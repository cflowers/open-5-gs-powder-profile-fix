#!/bin/bash

# print every command
set -x

#setup mongo
chmod +x /local/repository/scripts/mongoBuild.sh
sh  /local/repository/scripts/mongoBuild.sh

 ### Enable IPv4/IPv6 Forwarding
 sysctl -w net.ipv4.ip_forward=1
 sysctl -w net.ipv6.conf.all.forwarding=1
 ### Bind IP addresses that doesn't exist yet
 sysctl -w net.ipv4.ip_nonlocal_bind=1
 ### disable kernel sctp for now
 modprobe -rf sctp

### Add NAT Rule
sudo ip tuntap add name ogstun mode tun
sudo ip addr add 10.45.0.1/16 dev ogstun
sudo iptables -t nat -A POSTROUTING -s 10.45.0.0/16 ! -o ogstun -j MASQUERADE
sudo ip link set ogstun up

#setup 5G Core
chmod +x  /local/repository/scripts/open5gsBuild.sh
sh  /local/repository/scripts/open5gsBuild.sh
cd ..
pwd
sudo cp /local/repository/config/amf.yaml /etc/open5gs/amf.yaml
sudo cp /local/repository/config/smf.yaml /etc/open5gs/smf.yaml
sudo cp /local/repository/config/upf.yaml /etc/open5gs/upf.yaml
sudo cp /local/repository/config/nssf.yaml /etc/open5gs/nssf.yaml


chmod +x  /local/repository/scripts/open5gs-dbctl

# add default ue subscriber so user doesn't have to log into web ui
opc="E8ED289DEBA952E4283B54E88E6183CA"
newkey="465B5CE8B199B49FAA5F0A2EE238A6BC"
imsi="90170000000000"
apn="internet"
sst="2"
sd="000002"

cd /local/repository/scripts/

./open5gs-dbctl add_ue_with_slice $imsi $newkey $opc $apn $sst $sd

sudo systemctl restart open5gs-amfd.service
sudo systemctl restart open5gs-smfd.service
sudo systemctl restart open5gs-upfd.service
sudo systemctl restart open5gs-nssfd.service