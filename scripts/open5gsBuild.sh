#!/bin/bash

sudo apt-get update
#Install the dependencies for building the source code
sudo apt -y install python3-pip 
sudo apt -y install python3-setuptools 
sudo apt -y install python3-wheel 
sudo apt -y install ninja-build 
sudo apt -y install build-essential 
sudo apt -y install flex 
sudo apt -y install bison 
sudo apt -y install git
sudo apt -y install cmake 
sudo apt -y install libsctp-dev 
sudo apt -y install libgnutls28-dev 
sudo apt -y install libgcrypt-dev 
sudo apt -y install libssl-dev 
sudo apt -y install libidn11-dev 
sudo apt -y install libmongoc-dev 
sudo apt -y install libbson-dev 
sudo apt -y install libyaml-dev 
sudo apt -y install libnghttp2-dev 
sudo apt -y install libmicrohttpd-dev 
sudo apt -y install libcurl4-gnutls-dev 
sudo apt -y install libtins-dev 
sudo apt -y install libtalloc-dev 
sudo apt -y install meson


sudo add-apt-repository -y ppa:open5gs/latest
sudo apt -y update
sudo apt -y install open5gs




