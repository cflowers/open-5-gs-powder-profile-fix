!/bin/bash

pwd
git clone https://github.com/aligungr/UERANSIM

sudo apt -y update

sudo apt -y install gcc
sudo apt -y install g++
sudo apt -y install libsctp-dev 
sudo apt -y install lksctp-tools
sudo apt -y install iproute2

sudo npm install -g bower
sudo bower -v
sudo bower install bootstrap
sudo apt -y install build-essential 
sudo apt -y install libtool 
sudo apt - y install autoconf 
sudo apt -y install unzip 
sudo apt -y install wget

version=3.27
build=0


mkdir ~/temp
cd ~/temp
wget https://cmake.org/files/v$version/cmake-$version.$build.tar.gz
tar -xzvf cmake-$version.$build.tar.gz
cd cmake-$version.$build/

./bootstrap
make -j$(nproc)
sudo make install

cmake --version

cd /UERANSIM
pwd
sudo make

sudo cp /local/repository/config/open5gs-gnb.yaml config/open5gs-gnb.yaml
sudo cp /local/repository/config/open5gs-ue.yaml config/open5gs-ue.yaml