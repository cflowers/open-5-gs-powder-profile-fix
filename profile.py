#!/usr/bin/env python

#
# Standard geni-lib/portal libraries
#
import geni.portal as portal
import geni.rspec.pg as rspec
import geni.rspec.emulab as elab
import geni.rspec.igext as IG

tourDescription = """
This profile creates a 5G core via [Open5GS](https://github.com/open5gs/open5gs) and 
connects it to a simulated gNB Base Station and  User Equipment (UE) via [UERANSIM](https://github.com/aligungr/UERANSIM). 

Everything is set up automatically to be able to connect to a single UE that is associated to the network slice SST 2 and SD 000002.

The profile is known to work with UERANSIM v3.2.6 and latest Open5GS and Mongo 6.

"""

tourInstructions = """
To set up the default UE (or UEs, if you changed the parameter) and get internet access through it, do the following:

1. Wait until after the startup scripts have finished (in the POWDER experiment list view, the startup column for each node should say 'Finished'). 
This may take 15+ minutes.
2. Open up three terminals and ssh into the sim-ran box in all three, then run `cd /UERANSIM` in each.
3. In the first window, run `build/nr-gnb -c config/open5gs-gnb.yaml` to start the gNB. It should log a successful connection to the 5G core.
4. In the second window, run `build/nr-ue -c config/open5gs-ue.yaml` to connect the simulated UE to the gNB. It should log a successful connection, 
successful registration with the 5G core, and a PDU session message indicating the device is ready to communicate in the user plane. 
4. In the third window, run ping -I uesimtun0 google.com, you will see that the ue is able to ping the internet 
5. Then open up a fourth window into the open5gs box, and run tcpdump -i ogstun -n, you will see that traffic is going through the 
5g core when the ue is pinging

Refer to https://open5gs.org/open5gs/docs/guide/01-quickstart/ to learn how to modify the system, 
such as registering new UE subscribers in the core or modifying 5G network function configuration.

"""

#
# Globals
#
class GLOBALS(object):
    SITE_URN = "urn:publicid:IDN+emulab.net+authority+cm"
    UBUNTU20_IMG = "urn:publicid:IDN+emulab.net+image+emulab-ops//UBUNTU20-64-OSCN-U"
    CF_UERANSIM_IMG = "urn:publicid:IDN+emulab.net+image+NIWCLANT:CF_UERANSIM"

    # default type
    HWTYPE = "d430"
    SCRIPT_DIR = "/local/repository/scripts/"
    SCRIPT_CONFIG = "setup-config"


def invoke_script_str(filename):
    run_script = "sudo bash " + GLOBALS.SCRIPT_DIR + filename + " &> ~/install_script_output"
    return run_script

#
# This geni-lib script is designed to run in the PhantomNet Portal.
#
pc = portal.Context()

#
# Create our in-memory model of the RSpec -- the resources we're going
# to request in our experiment, and their configuration.
#
request = pc.makeRequestRSpec()

# Optional physical type for all nodes.
pc.defineParameter("phystype",  "Optional physical node type",
                   portal.ParameterType.STRING, "",
                   longDescription="Specify a physical node type (d430,d740,pc3000,d710,etc) " +
                   "instead of letting the resource mapper choose for you.")

# Retrieve the values the user specifies during instantiation.
params = pc.bindParameters()
pc.verifyParameters()

gNBCoreLink = request.Link("gNBCoreLink")

# Add node which will run gNodeB and UE components with a simulated RAN.
sim_ran = request.RawPC("sim-ran")
sim_ran.component_manager_id = GLOBALS.SITE_URN
sim_ran.disk_image = GLOBALS.CF_UERANSIM_IMG
sim_ran.hardware_type = params.phystype 
gNBCoreLink.addNode(sim_ran)

# Add node that will host the 5G Core Virtual Network Functions (AMF, SMF, UPF, etc).
open5gs = request.RawPC("open5gs")
open5gs.component_manager_id = GLOBALS.SITE_URN
open5gs.disk_image = GLOBALS.UBUNTU20_IMG
open5gs.addService(rspec.Execute(shell="bash", command=invoke_script_str("open5gs.sh")))
if params.phystype!=None:open5gs.hardware_type=params.phystype 
elif params.phystype==None:open5gs.hardware_type=GLOBALS.HWTYPE
gNBCoreLink.addNode(open5gs)

tour = IG.Tour()
tour.Description(IG.Tour.MARKDOWN, tourDescription)
tour.Instructions(IG.Tour.MARKDOWN, tourInstructions)
request.addTour(tour)


pc.printRequestRSpec(request)
