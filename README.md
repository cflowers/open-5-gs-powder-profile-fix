# open5gs-powder-profile

This is a [POWDER](https://powderwireless.net/) profile that automatically instantiates a full simulated 5g core network and UE RAN. It uses [Open5GS](https://github.com/open5gs/open5gs) for the 5c core on one node and uses [UERANSIM](https://github.com/aligungr/UERANSIM) to simulate a gNB and UE devices on another node
